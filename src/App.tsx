import { memo } from 'react'
import Container from '@mui/material/Container'

import Header from './components/Header'
import MessageBoard from './containers/MessageBoard'

const App = () => {
  return (
    <Container maxWidth="xl">
      <Header />
      <MessageBoard />
    </Container>
  )
}

export default memo(App)
