import { createTheme, responsiveFontSizes } from '@mui/material/styles'

const theme = createTheme({
  palette: {
    info: {
      main: '#88fca3',
    },
    warning: {
      main: '#fce788',
    },
    error: {
      main: '#f56236',
    },
  },
})

export default responsiveFontSizes(theme)
