import faker from 'faker'
import random from 'lodash/random'
import { nanoid } from 'nanoid'
import { Observable } from 'rxjs'
import { Message } from '../models/message.interface'
import { Priority } from '../models/priority.enum'

const observable = new Observable<Message>((subscriber) => {
  const generate = () => {
    const message = faker.lorem.sentence()
    const priority = random(0, 2) as Priority
    const nextInMS = random(500, 3000)
    subscriber.next({ message, priority, id: nanoid() })
    setTimeout(generate, nextInMS)
  }
  generate()
})

const subscribe = (callback: (message: Message) => void) => {
  const subscription = observable.subscribe({
    next: callback,
  })
  return () => subscription.unsubscribe()
}

export default subscribe
