import messageGeneratorService from '../messages-generator-service'

describe('services/messageGeneratorService', () => {
  it('should return a message', (done) => {
    messageGeneratorService.generate((message) => {
      expect(message).toEqual(
        expect.objectContaining({
          message: expect.any(String),
          priority: expect.any(Number),
          id: expect.any(String),
        })
      )
      done()
    })
  })

  it('should disconnect at any point', (done) => {
    let count = 0

    messageGeneratorService.generate(() => {
      count++
    })

    messageGeneratorService.stop(() => {
      expect(count).toBe(1)
      done()
    })
  })
})
