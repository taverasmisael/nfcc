import { GeneratorService } from '../models/generator-service.interface'
import { Message } from '../models/message.interface'
import generateMessage from './api'

const messageGeneratorService: () => GeneratorService<Message> = () => {
  let instance: (() => void) | null = null

  return {
    generate(cb) {
      instance = generateMessage(cb)
      return instance
    },
    stop(onDisconnect?: () => void) {
      if (instance) {
        instance()
        instance = null

        if (onDisconnect) {
          onDisconnect()
        }
      }
    },
  }
}

export default messageGeneratorService()
