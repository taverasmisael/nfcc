import produce from 'immer'
import {
  MessageAction,
  MessageActionType,
} from '../../models/message-action.interface'
import { MessagesState } from '../../models/messages-state.interface'

export default function MessagesReducer(
  state: MessagesState,
  action: MessageAction
): MessagesState {
  switch (action.type) {
    case MessageActionType.Add:
      return produce(state, (draft) => {
        draft.messages.splice(0, 0, action.payload)
      })
    case MessageActionType.Remove:
      return produce(state, (draft) => {
        draft.messages = draft.messages.filter((m) => m.id !== action.payload)
      })
    case MessageActionType.ClearAll:
      return produce(state, (draft) => {
        draft.messages = []
      })
    case MessageActionType.ToggleActive:
      return produce(state, (draft) => {
        draft.isActive = !draft.isActive
      })

    case MessageActionType.AddNotification:
      return produce(state, (draft) => {
        draft.notification = action.payload
      })
    case MessageActionType.RemoveNotification:
      return produce(state, (draft) => {
        draft.notification = undefined
      })
    default:
      return state
  }
}
