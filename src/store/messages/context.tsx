import {
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useReducer,
} from 'react'
import type { FC, Dispatch } from 'react'
import { MessageAction } from '../../models/message-action.interface'
import { MessagesState } from '../../models/messages-state.interface'
import reducer from './reducer'
import { Message } from '../../models/message.interface'
import { addMessage, addNotification } from './actions'
import { Priority } from '../../models/priority.enum'
import { GeneratorService } from '../../models/generator-service.interface'

const INITIAL_STATE: MessagesState = {
  messages: [],
  isActive: true,
}

export const MessagesStateContext = createContext<MessagesState>(INITIAL_STATE)
export const MessagesDispatchContext = createContext<Dispatch<MessageAction>>(
  {} as Dispatch<MessageAction>
)

export const MessagesContext: FC<MessagesContextProps> = ({
  children,
  initialState = INITIAL_STATE,
  generatorService: generator,
}) => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const isActive = useMemo(() => state.isActive, [state.isActive])

  const dispatchMessages = useCallback(
    (message: Message) => {
      if (message.priority === Priority.Error) {
        dispatch(addNotification(message))
      }
      dispatch(addMessage(message))
    },
    [dispatch]
  )

  useEffect(() => {
    if (!isActive) {
      generator.stop()
    } else {
      generator.generate(dispatchMessages)
    }

    return generator.stop
  }, [generator, isActive, dispatchMessages])

  return (
    <MessagesStateContext.Provider value={state}>
      <MessagesDispatchContext.Provider value={dispatch}>
        {children}
      </MessagesDispatchContext.Provider>
    </MessagesStateContext.Provider>
  )
}

interface MessagesContextProps {
  generatorService: GeneratorService<Message>
  initialState?: MessagesState
}
