import { createSelector } from 'reselect'
import filter from 'lodash/fp/filter'
import { Message } from '../../models/message.interface'
import { MessagesState } from '../../models/messages-state.interface'
import { Priority } from '../../models/priority.enum'

const getMessages = (state: MessagesState) => state.messages
const filterByPriority = (priority: Priority) =>
  filter((message: Message) => message.priority === priority)

export const getMessagesOfPriority = (priority: Priority) =>
  createSelector([getMessages], filterByPriority(priority))
