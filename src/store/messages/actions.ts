import {
  MessageAction,
  MessageActionType,
} from '../../models/message-action.interface'
import { Message } from '../../models/message.interface'

type ActionCreator<T = any> = () => MessageAction<T>
type ActionCreatorWithPayload<T> = (payload: T) => MessageAction<T>

export const toggleActive: ActionCreator = () => ({
  type: MessageActionType.ToggleActive,
})

export const addMessage: ActionCreatorWithPayload<Message> = (message) => ({
  type: MessageActionType.Add,
  payload: message,
})

export const addNotification: ActionCreatorWithPayload<Message> = (
  message
) => ({
  type: MessageActionType.AddNotification,
  payload: message,
})

export const removeMessage: ActionCreatorWithPayload<string> = (id) => ({
  type: MessageActionType.Remove,
  payload: id,
})

export const removeNotification: ActionCreator = () => ({
  type: MessageActionType.RemoveNotification,
})

export const clearAllMessages: ActionCreator = () => ({
  type: MessageActionType.ClearAll,
})
