import { MessagesState } from '../../../models/messages-state.interface'
import { Priority } from '../../../models/priority.enum'
import { getMessagesOfPriority } from '../selectors'

describe('store/messages/selectors', () => {
  const DEFAULT_STATE: MessagesState = {
    isActive: true,
    messages: [
      {
        id: '123',
        message: 'Nemo sed porro non placeat est ut quia.',
        priority: 0,
      },
      {
        id: '456',
        message:
          'Soluta earum facere qui laborum dignissimos omnis accusantium eum nisi.',
        priority: 1,
      },
      {
        id: '789',
        message:
          'Voluptas at aut dolor explicabo inventore vel sint aut omnis.',
        priority: 2,
      },
      { id: '987', message: 'Adipisci et qui.', priority: 2 },
    ],
  }

  it('should return all messages of Priority.Error', () => {
    const result = getMessagesOfPriority(Priority.Error)(DEFAULT_STATE)
    expect(result).toHaveLength(1)
  })
  it('should return all messages of Priority.Warn', () => {
    const result = getMessagesOfPriority(Priority.Warn)(DEFAULT_STATE)
    expect(result).toHaveLength(1)
  })
  it('should return all messages of Priority.Info', () => {
    const result = getMessagesOfPriority(Priority.Info)(DEFAULT_STATE)
    expect(result).toHaveLength(2)
  })
})
