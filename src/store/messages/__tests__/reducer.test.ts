import merge from 'lodash/fp/merge'
import {
  MessageAction,
  MessageActionType,
} from '../../../models/message-action.interface'
import { MessagesState } from '../../../models/messages-state.interface'
import messagesReducer from '../reducer'

describe('store/messages/reducer', () => {
  const DEFAULT_STATE: MessagesState = {
    isActive: true,
    messages: [
      {
        id: '123',
        message: 'Nemo sed porro non placeat est ut quia.',
        priority: 0,
      },
      {
        id: '456',
        message:
          'Soluta earum facere qui laborum dignissimos omnis accusantium eum nisi.',
        priority: 1,
      },
      {
        id: '789',
        message:
          'Voluptas at aut dolor explicabo inventore vel sint aut omnis.',
        priority: 2,
      },
      { id: '987', message: 'Adipisci et qui.', priority: 2 },
    ],
  }

  const dispatch = (
    action: MessageAction,
    extraState: Partial<MessagesState> = {}
  ) => messagesReducer(merge(DEFAULT_STATE, extraState), action)

  it('should return the default state', () => {
    let result = dispatch({} as any)
    expect(result).toEqual(DEFAULT_STATE)
    result = dispatch({ type: 'unknwon' } as any)
    expect(result).toEqual(DEFAULT_STATE)
  })

  it('should add a message', () => {
    const NEW_MESSAGE = {
      id: '345',
      message: 'Nemo sed porro non placeat est ut quia.',
      priority: 0,
    }
    const result = dispatch({
      type: MessageActionType.Add,
      payload: NEW_MESSAGE,
    })

    expect(result.messages).toHaveLength(DEFAULT_STATE.messages.length + 1)
    expect(result.messages[0]).toEqual(NEW_MESSAGE)
  })

  it('should remove an specific message', () => {
    const idToRemove = '123'
    const result = dispatch({
      type: MessageActionType.Remove,
      payload: idToRemove,
    })
    expect(result.messages).toHaveLength(DEFAULT_STATE.messages.length - 1)

    expect(result.messages.find((m) => m.id === idToRemove)).toBeUndefined()
  })
  it('should remove all messages', () => {
    const result = dispatch({ type: MessageActionType.ClearAll })
    expect(result.messages).toHaveLength(0)
  })
  it('should toggle active status', () => {
    let result = dispatch({ type: MessageActionType.ToggleActive })
    expect(result.isActive).toBe(false)

    result = dispatch(
      { type: MessageActionType.ToggleActive },
      { isActive: false }
    )
    expect(result.isActive).toBe(true)
  })

  it('should add messages as a notification', () => {
    const NEW_MESSAGE = {
      id: '456',
      message: 'Nemo sed porr non placeat est ut quia.',
      priority: 0,
    }
    const result = dispatch({
      type: MessageActionType.AddNotification,
      payload: NEW_MESSAGE,
    })

    expect(result.notification).toEqual(NEW_MESSAGE)
  })
  it('should remove the notification message', () => {
    const NOTIFICATION = {
      id: '456',
      message: 'Nemo sed porr non placeat est ut quia.',
      priority: 0,
    }
    const result = dispatch(
      { type: MessageActionType.RemoveNotification },
      { notification: NOTIFICATION }
    )

    expect(result.notification).toBeUndefined()
  })
})
