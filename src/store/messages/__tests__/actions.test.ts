import { Priority } from '../../../models/priority.enum'
import { Message } from '../../../models/message.interface'
import {
  addMessage,
  addNotification,
  clearAllMessages,
  removeMessage,
  removeNotification,
  toggleActive,
} from '../actions'

describe('store/messages/actions', () => {
  const message: Message = {
    id: '1',
    message: 'Text',
    priority: Priority.Warn,
  }

  it('can create an action to add message', () => {
    const expectedAction = addMessage(message)

    expect(expectedAction).toMatchSnapshot()
  })
  it('can create an naction to add a notification message', () => {
    const expectedAction = addNotification(message)

    expect(expectedAction).toMatchSnapshot()
  })

  it('can create an action to add a notification message', () => {
    const expectedAction = clearAllMessages()

    expect(expectedAction).toMatchSnapshot()
  })

  it('can create an action to remove a message', () => {
    const expectedAction = removeMessage('1')

    expect(expectedAction).toMatchSnapshot()
  })
  it('can create an action to remove a notification message', () => {
    const expectedAction = removeNotification()

    expect(expectedAction).toMatchSnapshot()
  })
  it('can create an action to toggle the active status', () => {
    const expectedAction = toggleActive()

    expect(expectedAction).toMatchSnapshot()
  })
})
