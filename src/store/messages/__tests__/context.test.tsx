import { useContext } from 'react'
import { render } from '@testing-library/react'

import { MessagesStateContext, MessagesContext } from '../context'
import { Priority } from '../../../models/priority.enum'

const ERROR_MESSAGE = { id: '1', message: 'Message', priority: Priority.Error }

const generatorMock = {
  generate: (cb: any) => {
    cb(ERROR_MESSAGE)
    return () => {}
  },
  stop: jest.fn(),
}

const Component = () => {
  const state = useContext(MessagesStateContext)

  return (
    <div data-testid="container">
      <div data-testid="state">{JSON.stringify(state)}</div>
    </div>
  )
}

describe('store/messages/context', () => {
  describe('MessagesContext', () => {
    it('provides the correct state', () => {
      const { container, getByTestId } = render(
        <MessagesContext generatorService={generatorMock}>
          <Component />
        </MessagesContext>
      )

      expect(container).toMatchSnapshot()

      // This is a rough check to see if the state is correct (and if we are adding a notification)
      expect(getByTestId('state')).toHaveTextContent(
        JSON.stringify({
          messages: [ERROR_MESSAGE],
          isActive: true,
          notification: ERROR_MESSAGE,
        })
      )
    })
  })
})
