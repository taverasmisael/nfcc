import { memo, SyntheticEvent, useCallback, useContext } from 'react'
import IconButton from '@mui/material/IconButton'
import Snackbar from '@mui/material/Snackbar'
import Alert from '@mui/material/Alert'
import CloseIcon from '@mui/icons-material/Close'

import {
  MessagesStateContext,
  MessagesDispatchContext,
} from '../../store/messages/context'
import { removeNotification } from '../../store/messages/actions'

const NOTIFICATION_POSITION = { vertical: 'top', horizontal: 'right' } as const

function Notification() {
  const { notification } = useContext(MessagesStateContext)
  const dispatch = useContext(MessagesDispatchContext)
  const onClose = useCallback(
    (e?: SyntheticEvent, reason?: string) => {
      if (reason === 'clickaway') return
      dispatch(removeNotification())
    },
    [dispatch]
  )

  return (
    <Snackbar
      key={notification?.id}
      open={!!notification}
      autoHideDuration={2000}
      onClose={onClose}
      anchorOrigin={NOTIFICATION_POSITION}
      action={
        <IconButton
          size="small"
          aria-label="close"
          color="inherit"
          onClick={onClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      }
    >
      <Alert onClose={onClose} sx={{ width: '100%' }} severity="error">
        {notification?.message}
      </Alert>
    </Snackbar>
  )
}

export default memo(Notification)
