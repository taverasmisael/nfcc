import { fireEvent } from '@testing-library/react'
import { Priority } from '../../../models/priority.enum'
import { removeNotification } from '../../../store/messages/actions'
import { renderWithContextAndTheme } from '../../../utils/testUtils'

import Notification from '../Notification'

describe('container/Notification', () => {
  it('looks as expected', () => {
    const { container } = renderWithContextAndTheme(<Notification />, {
      messages: [],
      isActive: true,
      notification: { id: '3', message: 'test', priority: Priority.Error },
    })

    expect(container.firstChild).toMatchSnapshot()
  })
  it('works as expected', () => {
    const { getByLabelText, dispatch } = renderWithContextAndTheme(
      <Notification />,
      {
        messages: [],
        isActive: true,
        notification: { id: '3', message: 'test', priority: Priority.Error },
      }
    )

    fireEvent.click(getByLabelText('Close'))
    expect(dispatch).toHaveBeenCalledWith(removeNotification())
  })
})
