import Container from '@mui/material/Container'
import styled from 'styled-components'

import { MessagesContext } from '../../store/messages/context'
import messageGeneratorService from '../../services/messages-generator-service'
import MessageList from '../../components/MessageList'
import { Priority } from '../../models/priority.enum'
import Controls from './MessageBoardControls'
import Notification from '../Notification'

function MessageBoard() {
  return (
    <Container>
      <MessagesContext generatorService={messageGeneratorService}>
        <Notification />
        <Controls />
        <ListContainer data-testid="list">
          <MessageList priority={Priority.Error} />
          <MessageList priority={Priority.Warn} />
          <MessageList priority={Priority.Info} />
        </ListContainer>
      </MessagesContext>
    </Container>
  )
}

export default MessageBoard

const ListContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: ${(props) => props.theme.spacing(1.25)};
  margin-top: ${(props) => props.theme.spacing(2)}; ;
`
