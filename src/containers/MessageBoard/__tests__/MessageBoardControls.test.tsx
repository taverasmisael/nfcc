import { fireEvent } from '@testing-library/react'
import { clearAllMessages, toggleActive } from '../../../store/messages/actions'
import { renderWithContextAndTheme } from '../../../utils/testUtils'
import MessageBoardControls from '../MessageBoardControls'

describe('component/MessageBoardControls', () => {
  it('looks as expected', () => {
    const { container } = renderWithContextAndTheme(<MessageBoardControls />)
    expect(container.firstChild).toMatchSnapshot()
  })

  it('works as expected', () => {
    const { getByTestId, dispatch } = renderWithContextAndTheme(
      <MessageBoardControls />
    )
    const actionBtn = getByTestId('action')
    const clearAllBtn = getByTestId('clear')

    expect(actionBtn).toHaveTextContent('Stop')

    // Toggle isActive
    fireEvent.click(actionBtn)
    expect(dispatch).toHaveBeenCalledWith(toggleActive())

    // Clear all messages
    fireEvent.click(clearAllBtn)
    expect(dispatch).toHaveBeenCalledWith(clearAllMessages())

    expect(dispatch).toHaveBeenCalledTimes(2)
  })

  it('has the right text when isActive is false', () => {
    const { getByTestId } = renderWithContextAndTheme(
      <MessageBoardControls />,
      { messages: [], isActive: false }
    )
    expect(getByTestId('action')).toHaveTextContent('Start')
  })
})
