import { renderWithTheme } from '../../../utils/testUtils'
import MessageBoard from '../MessageBoard'

jest.mock('../../../services/messages-generator-service')

describe('container/MessageBoard', () => {
  it('looks as expected', () => {
    const { container, getByTestId } = renderWithTheme(<MessageBoard />)
    expect(getByTestId('list').children).toHaveLength(3)
    expect(container).toMatchSnapshot()
  })
})
