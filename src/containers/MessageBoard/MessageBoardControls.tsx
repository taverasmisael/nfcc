import { useCallback, useContext, memo } from 'react'
import Container from '@mui/material/Container'
import Button from '@mui/material/Button'
import styled from 'styled-components'
import {
  MessagesDispatchContext,
  MessagesStateContext,
} from '../../store/messages/context'
import { clearAllMessages, toggleActive } from '../../store/messages/actions'

function MessageBoardControls() {
  const { isActive } = useContext(MessagesStateContext)
  const dispatch = useContext(MessagesDispatchContext)

  const onToggleActive = useCallback(() => {
    dispatch(toggleActive())
  }, [dispatch])

  const onClearAll = useCallback(() => {
    dispatch(clearAllMessages())
  }, [dispatch])

  const actionText = isActive ? 'Stop' : 'Start'

  return (
    <Container>
      <Grid>
        <Button
          data-testid="action"
          variant="contained"
          color="info"
          onClick={onToggleActive}
        >
          {actionText}
        </Button>
        <Button
          data-testid="clear"
          variant="contained"
          color="info"
          onClick={onClearAll}
        >
          Clear all
        </Button>
      </Grid>
    </Container>
  )
}

const Grid = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: ${(props) => props.theme.spacing(1)}; ;
`

export default memo(MessageBoardControls)
