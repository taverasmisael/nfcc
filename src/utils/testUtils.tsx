import type { ReactElement } from 'react'
import { ThemeProvider } from 'styled-components'
import { render } from '@testing-library/react'
import {
  MessagesDispatchContext,
  MessagesStateContext,
} from '../store/messages/context'

import theme from '../theme'
import { MessagesState } from '../models/messages-state.interface'

export const renderWithTheme = (component: ReactElement<any>) => {
  return render(<ThemeProvider theme={theme}>{component}</ThemeProvider>)
}

export const renderWithContext = (
  component: ReactElement<any>,
  initialState: MessagesState = { messages: [], isActive: true }
) => {
  const dispatch = jest.fn()
  const renderResult = render(
    <MessagesStateContext.Provider value={initialState}>
      <MessagesDispatchContext.Provider value={dispatch}>
        {component}
      </MessagesDispatchContext.Provider>
    </MessagesStateContext.Provider>
  )
  return {
    ...renderResult,
    dispatch,
  }
}

export const renderWithContextAndTheme = (
  component: ReactElement<any>,
  initialState: MessagesState = { messages: [], isActive: true }
) => {
  const dispatch = jest.fn()
  const renderResult = render(
    <ThemeProvider theme={theme}>
      <MessagesStateContext.Provider value={initialState}>
        <MessagesDispatchContext.Provider value={dispatch}>
          {component}
        </MessagesDispatchContext.Provider>
      </MessagesStateContext.Provider>
    </ThemeProvider>
  )
  return {
    ...renderResult,
    dispatch,
  }
}
