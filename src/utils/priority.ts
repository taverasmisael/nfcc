import { Priority } from '../models/priority.enum'

export const getPriorityLabel = (
  priority: Priority,
  isSafe?: boolean
): string => {
  switch (priority) {
    case Priority.Error:
      return 'Error'
    case Priority.Warn:
      return isSafe ? 'Warn' : 'Warning'
    case Priority.Info:
      return 'Info'
    default:
      return 'Unknown'
  }
}
