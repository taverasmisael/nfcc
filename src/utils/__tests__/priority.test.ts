import { Priority } from '../../models/priority.enum'
import { getPriorityLabel } from '../priority'

describe('utils/priority', () => {
  describe('getPriorityLabel', () => {
    it('returns the right string for knwon priority levels', () => {
      expect(getPriorityLabel(Priority.Error)).toBe('Error')
      expect(getPriorityLabel(Priority.Warn)).toBe('Warning')
      expect(getPriorityLabel(Priority.Info)).toBe('Info')
    })

    it('returns a `Unknown` value for unknwon priority levels', () => {
      expect(getPriorityLabel(3)).toBe('Unknown')
      expect(getPriorityLabel(4)).toBe('Unknown')
      expect(getPriorityLabel(123)).toBe('Unknown')
    })

    it('returns a safe version of Priority.Warn string', () => {
      // This is usefull when using the string as a class name / palette color
      expect(getPriorityLabel(Priority.Warn, true)).toBe('Warn')
    })
  })
})
