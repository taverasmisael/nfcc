import { memo, useCallback, useContext } from 'react'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import MuiButton from '@mui/material/Button'
import styled from 'styled-components'
import { Priority } from '../../models/priority.enum'
import { getPriorityLabel } from '../../utils/priority'
import { MessagesDispatchContext } from '../../store/messages/context'
import { removeMessage } from '../../store/messages/actions'

function Message(props: MessageProps) {
  const dispatch = useContext(MessagesDispatchContext)
  const onClick = useCallback(
    () => dispatch(removeMessage(props.id)),
    [dispatch, props.id]
  )
  return (
    <MessageContainer elevation={2} priority={props.priority}>
      <Typography>{props.message}</Typography>
      <ActionContainer>
        <Button size="small" onClick={onClick}>
          Clear
        </Button>
      </ActionContainer>
    </MessageContainer>
  )
}

export default memo(Message)

const MessageContainer = styled(Paper)<{ priority: Priority }>`
  background-color: ${(props) =>
    props.theme.palette[getPriorityLabel(props.priority).toLowerCase()].main};
  padding: ${(props) => props.theme.spacing(2)};

  & + & {
    margin-top: ${(props) => props.theme.spacing(1.5)};
  }
`

const Button = styled(MuiButton)`
  color: ${(props) => props.theme.palette.common.black};
  text-transform: none;
`
const ActionContainer = styled.div`
  text-align: right;
`

interface MessageProps {
  id: string
  message: string
  priority: Priority
}
