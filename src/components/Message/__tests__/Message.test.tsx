import { fireEvent } from '@testing-library/react'
import { Priority } from '../../../models/priority.enum'
import { removeMessage } from '../../../store/messages/actions'
import { renderWithContextAndTheme } from '../../../utils/testUtils'
import theme from '../../../theme'
import Message from '../Message'

describe('component/Message', () => {
  describe('Visuals', () => {
    it('should render correctly', () => {
      const { container } = renderWithContextAndTheme(
        <Message message="My message" id="3" priority={Priority.Error} />
      )
      expect(container.firstChild).toMatchSnapshot()
    })

    it('can be Priority.Error', () => {
      const { container } = renderWithContextAndTheme(
        <Message message="My message" id="3" priority={Priority.Error} />
      )

      expect(container.firstChild).toHaveStyleRule(
        'background-color',
        theme.palette.error.main
      )
    })
    it('can be Priority.Warn', () => {
      const { container } = renderWithContextAndTheme(
        <Message message="My message" id="3" priority={Priority.Warn} />
      )
      expect(container.firstChild).toHaveStyleRule(
        'background-color',
        theme.palette.warning.main
      )
    })
    it('can be Priority.Info', () => {
      const { container } = renderWithContextAndTheme(
        <Message message="My message" id="3" priority={Priority.Info} />
      )
      expect(container.firstChild).toHaveStyleRule(
        'background-color',
        theme.palette.info.main
      )
    })
  })

  describe('Functionality', () => {
    it('dispatches the right action on clear', () => {
      const { getByText, dispatch } = renderWithContextAndTheme(
        <Message message="My message" id="3" priority={Priority.Error} />
      )
      const button = getByText('Clear')
      fireEvent.click(button)
      expect(dispatch).toHaveBeenCalledWith(removeMessage('3'))
    })
  })
})
