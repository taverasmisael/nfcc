import { useMemo, useContext, memo } from 'react'
import Typography from '@mui/material/Typography'
import styled from 'styled-components'

import { MessagesStateContext } from '../../store/messages/context'
import { Priority } from '../../models/priority.enum'
import { getPriorityLabel } from '../../utils/priority'
import Message from '../Message'
import { getMessagesOfPriority } from '../../store/messages/selectors'

const getListTitle = (priority: Priority) => {
  const label = getPriorityLabel(priority)
  return `${label} Type ${priority + 1}`
}

function MessageList(props: MessageListProps) {
  const state = useContext(MessagesStateContext)

  const messages = getMessagesOfPriority(props.priority)(state)
  const title = useMemo(() => getListTitle(props.priority), [props.priority])

  return (
    <div>
      <Header>
        <Typography variant="h6" component="h4">
          {title}
        </Typography>
        <Typography variant="body2" component="span" data-testid="count">
          Count {messages.length}
        </Typography>
      </Header>
      <Content data-testid="content">
        {messages.map((m) => (
          <Message
            key={m.id}
            id={m.id}
            message={m.message}
            priority={m.priority}
          />
        ))}
      </Content>
    </div>
  )
}

export default memo(MessageList)

const Header = styled.div`
  margin-bottom: ${(props) => props.theme.spacing(1)};
`
const Content = styled.div`
  height: 70vh;
  overflow: auto;
`

interface MessageListProps {
  priority: Priority
}
