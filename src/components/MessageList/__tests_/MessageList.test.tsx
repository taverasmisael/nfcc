import { Priority } from '../../../models/priority.enum'
import { renderWithContextAndTheme } from '../../../utils/testUtils'
import MessageList from '../MessageList'

describe('components/MessageList', () => {
  it('renders correctly with no messages', () => {
    const errorList = renderWithContextAndTheme(
      <MessageList priority={Priority.Error} />,
      { messages: [], isActive: true }
    )
    const warnList = renderWithContextAndTheme(
      <MessageList priority={Priority.Warn} />,
      { messages: [], isActive: true }
    )
    const infoList = renderWithContextAndTheme(
      <MessageList priority={Priority.Info} />,
      { messages: [], isActive: true }
    )

    expect(errorList.container.firstChild).toMatchSnapshot()
    expect(warnList.container.firstChild).toMatchSnapshot()
    expect(infoList.container.firstChild).toMatchSnapshot()
  })

  it('renders correctly with messages', () => {
    const { container, getByTestId } = renderWithContextAndTheme(
      <MessageList priority={Priority.Info} />,
      {
        messages: [
          { priority: Priority.Info, message: 'Some Info message 1', id: '1' },
          { priority: Priority.Info, message: 'Some Info message 2', id: '2' },
          { priority: Priority.Error, message: 'Some Error message', id: '3' },
        ],
        isActive: true,
      }
    )

    const messages = getByTestId('content').children
    const count = getByTestId('count')

    expect(messages).toHaveLength(2)
    expect(count).toHaveTextContent('Count 2')
    expect(container).toMatchSnapshot()
  })
})
