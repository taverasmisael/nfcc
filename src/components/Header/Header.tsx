import { memo } from 'react'
import Container from '@mui/material/Container'
import Divider from '@mui/material/Divider'
import Typography from '@mui/material/Typography'
import styled from 'styled-components'

function Header() {
  return (
    <Container component={HeaderContainer} maxWidth="xl">
      <Typography variant="h4" component="h1">
        nunffsaid.com Coding Challenge
      </Typography>
      <Divider></Divider>
    </Container>
  )
}

const HeaderContainer = styled.header`
  margin-bottom:  ${(props) => props.theme.spacing(3)};;
`

export default memo(Header)
