import { renderWithTheme } from '../../../utils/testUtils'
import Header from '../Header'

describe('component/Header', () => {
  it('works as expected', () => {
    const { container } = renderWithTheme(<Header />)

    expect(container.firstChild).toMatchSnapshot()
  })
})
