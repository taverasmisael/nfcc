export const mockMessages = [
  { message: 'Nemo sed porro non placeat est ut quia.', priority: 0 },
  {
    message:
      'Soluta earum facere qui laborum dignissimos omnis accusantium eum nisi.',
    priority: 1,
  },
  {
    message: 'Voluptas at aut dolor explicabo inventore vel sint aut omnis.',
    priority: 2,
  },
  { message: 'Adipisci et qui.', priority: 2 },
  {
    message:
      'Itaque consequatur quae qui exercitationem perspiciatis adipisci nulla.',
    priority: 1,
  },
  { message: 'Dolorem non et esse.', priority: 0 },
  { message: 'Officia nisi tempora quam.', priority: 1 },
  {
    message:
      'Sunt voluptas molestiae mollitia soluta quis non repudiandae quis.',
    priority: 2,
  },
  { message: 'Ut maxime nesciunt a nemo enim debitis.', priority: 2 },
  { message: 'Deleniti illum libero.', priority: 0 },
  { message: 'Dolor corrupti autem sunt qui.', priority: 1 },
  { message: 'Qui quo animi exercitationem sint et nemo et.', priority: 1 },
  {
    message: 'Repellat sit fugiat sint vero mollitia culpa modi ut quas.',
    priority: 0,
  },
  { message: 'Sit et blanditiis fuga sed.', priority: 2 },
  { message: 'Vero aspernatur quo optio.', priority: 1 },
  { message: 'Harum aliquid atque eos voluptatum.', priority: 2 },
  { message: 'Maxime et aut inventore.', priority: 2 },
  {
    message: 'Voluptatem est aliquid tenetur suscipit rem dolore dolorem.',
    priority: 1,
  },
  { message: 'Sint laboriosam assumenda voluptatibus esse.', priority: 2 },
  { message: 'Rerum et quo ullam vitae fugiat magni sint.', priority: 2 },
  { message: 'Aut in ea.', priority: 1 },
  { message: 'Laborum blanditiis iure.', priority: 2 },
  {
    message: 'Tenetur ipsam veniam sint mollitia sapiente blanditiis earum.',
    priority: 0,
  },
  {
    message: 'Voluptas eos tempore reprehenderit velit ad cum in sed.',
    priority: 0,
  },
  {
    message: 'Et tempore qui voluptas id impedit vitae ad aut eum.',
    priority: 0,
  },
  { message: 'Eum quia est.', priority: 2 },
  {
    message:
      'Porro repellat numquam mollitia occaecati eos consectetur excepturi.',
    priority: 2,
  },
  { message: 'Nam ea qui sit soluta enim.', priority: 0 },
  { message: 'Omnis odit architecto illum quae tempora eaque.', priority: 2 },
  { message: 'Illum repellat sequi.', priority: 2 },
  { message: 'A cupiditate qui harum harum nam mollitia et.', priority: 0 },
  { message: 'Earum atque expedita et ea.', priority: 1 },
  { message: 'Magnam tempora in et hic aut repudiandae.', priority: 0 },
  { message: 'Eaque fugit quas vero at.', priority: 0 },
  { message: 'Esse quia ratione.', priority: 0 },
  { message: 'Qui suscipit dignissimos ullam.', priority: 1 },
  {
    message: 'Voluptas alias non blanditiis omnis voluptate omnis.',
    priority: 2,
  },
  {
    message:
      'Assumenda et aut qui placeat atque voluptatem ad officia reprehenderit.',
    priority: 1,
  },
  {
    message: 'Delectus et reiciendis reiciendis et porro quasi aut et.',
    priority: 0,
  },
  { message: 'Minus et voluptatem non.', priority: 1 },
  { message: 'Harum dicta nesciunt qui.', priority: 2 },
  { message: 'Deleniti tenetur aut optio ducimus et.', priority: 1 },
  {
    message: 'Voluptas eum nobis id aut dolores pariatur corrupti qui.',
    priority: 2,
  },
  {
    message: 'Eum non quod et dolores voluptatem inventore qui aperiam.',
    priority: 0,
  },
  { message: 'Quaerat molestias sit facere eaque.', priority: 1 },
  { message: 'Qui optio quia aliquid dolorem.', priority: 0 },
  {
    message:
      'Quod dignissimos illo exercitationem error consequatur qui beatae aut.',
    priority: 2,
  },
  { message: 'Fuga temporibus dolorum inventore quo.', priority: 1 },
  { message: 'Libero nihil beatae.', priority: 0 },
  { message: 'Sed reprehenderit voluptas.', priority: 0 },
  { message: 'Aut doloremque officia placeat animi.', priority: 2 },
  {
    message: 'Vel esse reprehenderit ea qui aut sint voluptatem veniam.',
    priority: 1,
  },
  { message: 'Dolor voluptatem voluptatibus eum sunt.', priority: 0 },
  {
    message: 'Eligendi unde unde iusto porro iure qui perferendis et.',
    priority: 2,
  },
  { message: 'Velit quo cum est inventore velit necessitatibus.', priority: 2 },
  {
    message:
      'Culpa ullam non numquam pariatur consequatur et accusamus in harum.',
    priority: 0,
  },
  {
    message: 'Error et occaecati ipsa non mollitia explicabo in quo numquam.',
    priority: 2,
  },
  { message: 'Ea aut tempora hic.', priority: 2 },
  { message: 'Dolores voluptatem suscipit temporibus.', priority: 2 },
  { message: 'Aliquid voluptatem molestiae aperiam.', priority: 1 },
  { message: 'Totam voluptas alias totam praesentium.', priority: 0 },
  { message: 'Sed pariatur tempore aut et.', priority: 0 },
  { message: 'Voluptate nihil occaecati iure dolor aut ut et.', priority: 1 },
  { message: 'Dolores eos inventore et.', priority: 1 },
  { message: 'Animi et et quibusdam voluptate facere non.', priority: 1 },
  {
    message:
      'Animi ea repudiandae minus voluptatum asperiores autem totam vitae sit.',
    priority: 2,
  },
  { message: 'Porro dolorem aperiam aliquid.', priority: 1 },
  { message: 'Est perferendis possimus hic nam eius.', priority: 0 },
  { message: 'Enim saepe in voluptas facere voluptatem.', priority: 0 },
  { message: 'Magni saepe facere excepturi in laudantium.', priority: 0 },
  {
    message:
      'Quibusdam sed reprehenderit alias ad et quis sint minus exercitationem.',
    priority: 2,
  },
  { message: 'Deserunt quia sapiente vel eius perferendis.', priority: 2 },
  {
    message: 'Earum ut dolorum et dicta deserunt placeat aliquid voluptatem.',
    priority: 1,
  },
  { message: 'Esse hic et minima omnis expedita.', priority: 2 },
  { message: 'Enim vitae vel vero voluptas est corporis non.', priority: 2 },
  { message: 'Eligendi accusantium voluptates ut.', priority: 1 },
  { message: 'Rem ut nobis vel.', priority: 0 },
  {
    message: 'Modi et id est suscipit laudantium omnis perspiciatis laborum.',
    priority: 2,
  },
  { message: 'Modi nisi veniam itaque eos illum.', priority: 0 },
  {
    message: 'Eius ipsam nostrum doloremque dolorem reiciendis qui.',
    priority: 0,
  },
  {
    message:
      'Quo veniam molestiae magni nemo explicabo laboriosam odit placeat commodi.',
    priority: 1,
  },
  { message: 'Quia et qui.', priority: 0 },
  {
    message: 'Repellat eaque hic esse laboriosam dolores consequatur.',
    priority: 0,
  },
  {
    message:
      'Voluptas doloremque autem nulla eius minus distinctio ut odio eligendi.',
    priority: 2,
  },
  { message: 'Autem aliquam harum culpa nihil delectus.', priority: 0 },
  { message: 'Officia non ducimus consequatur.', priority: 0 },
  { message: 'Tempora soluta vero quia qui enim.', priority: 1 },
  {
    message: 'Adipisci et quia repellat architecto inventore velit ea.',
    priority: 0,
  },
  {
    message: 'Autem quo accusamus dolor animi et dolor aut aut vitae.',
    priority: 0,
  },
  { message: 'Quas facilis non id.', priority: 0 },
  {
    message: 'Impedit quidem est accusamus reiciendis et quia sunt adipisci.',
    priority: 2,
  },
  { message: 'Est architecto enim eos ea.', priority: 1 },
  { message: 'Est dolores veritatis consequatur corporis quo.', priority: 1 },
  { message: 'At sunt corrupti quos quae rem.', priority: 1 },
  { message: 'Officia quia eum est sunt.', priority: 0 },
  { message: 'Delectus maxime voluptatem cumque saepe.', priority: 2 },
  { message: 'Quasi vel aut quia.', priority: 1 },
  { message: 'Facilis sed excepturi eos ut eligendi natus.', priority: 0 },
  { message: 'Cum expedita voluptatem architecto.', priority: 2 },
  { message: 'Non fuga modi odit.', priority: 2 },
  { message: 'Voluptas nihil inventore non vel harum.', priority: 0 },
  { message: 'Vitae ducimus enim ad sed.', priority: 2 },
  { message: 'Ut impedit recusandae aliquam autem quibusdam.', priority: 0 },
  {
    message: 'Vitae aut rerum dolorem ad quaerat perspiciatis fuga veritatis.',
    priority: 1,
  },
  { message: 'Eaque quos fugiat ad consequatur rem in ipsum.', priority: 1 },
  {
    message: 'Aliquam laboriosam commodi fugit enim et libero cupiditate.',
    priority: 0,
  },
]
