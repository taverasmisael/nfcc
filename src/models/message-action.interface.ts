export interface MessageAction<T = any> {
  type: MessageActionType
  payload?: T
}

export enum MessageActionType {
  Add = '[MESSAGES]:ADD',
  AddNotification = '[MESSAGES]:ADD_NOTIFICATION',
  Remove = '[MESSAAGES]:REMOVE',
  RemoveNotification = '[MESSAAGES]:REMOVE_NOTIFICATION',
  ToggleActive = '[MESSAAGES]:TOGGLE_ACTIVE',
  ClearAll = '[MESSAGES]:CLEAR_ALL',
}
