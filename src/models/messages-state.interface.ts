import { Message } from './message.interface'

export interface MessagesState {
  messages: Message[]
  isActive: boolean
  notification?: Message
}
