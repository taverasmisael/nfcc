export interface GeneratorService<T> {
  generate(callback: (data: T) => void): () => void
  stop(onDisconnect?: Function): void
}
