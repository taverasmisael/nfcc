import { Priority } from './priority.enum'

export interface Message {
  id: string
  message: string
  priority: Priority
}
